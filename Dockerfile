FROM flightpos/node:14-jdk8

# Create app directory
WORKDIR /usr/lambda/app

RUN apt-get update 

# Install serverless
RUN npm install -g serverless
RUN apt-get clean;

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Set up the Node dependencies
RUN npm install

# Bundle app source
COPY . .

# Install DynamoDB Local
RUN serverless dynamodb install


EXPOSE 3000
EXPOSE 8000

# Uses a pre-existing dynamodb
CMD [ "npm", "run" , "sls" ]
