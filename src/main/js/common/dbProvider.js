/**
 * Encapsulate connection to DynamoDB, using parameters from config.js
 * Needs DYNAMO_SERVER=localhost as an environment to configure.
 * For some reason the serverless variable is not present. 
 * 
 * Note: an environment variable IS_OFFLINE needs to be set to  true when running offline.
 * This was being done by the serverless-offline plugin but a current version does not seem to be setting this.
 */

const AWS = require('aws-sdk');
const config = {
	port: 8000 ,
	region: 'us-east-1'
};
const tables= {
	events: "events"
}
/* Set up by a call to createClientConfig() */
var clientConfig ;

console.log ("dbProvider -------------------------------------------");
//console.log (`process.env`,process.env);

function initDefaultConfig() {
	
	console.log (`dbProvider - initDefaultConfig`);

	if ( process.env.AWS_DEFAULT_REGION !== undefined ) config.region = process.env.AWS_DEFAULT_REGION ;
	if ( process.env.DYNAMO_SERVER !== undefined ) config.server = process.env.DYNAMO_SERVER ;
	if ( process.env.DYNAMO_PORT !== undefined ) config.port = process.env.DYNAMO_PORT ;	
	if ( process.env.AWS_DYNAMO_ENDPOINT !== undefined ) config.endpoint = process.env.AWS_DYNAMO_ENDPOINT ;
	if ( process.env.tableName !== undefined ) tables.events = process.env.tableName ;
}

function createClientConfig() {

	console.log (`dbProvider - createClientConfig`);
	if ( clientConfig ) return clientConfig ;
	
	try {
		// Environment validation
		initDefaultConfig();

		if ( process.env.IS_OFFLINE ) {

			console.log (`dbProvider - IS_OFFLINE ---------------------`);

			if ( !config.endpoint ) {

				console.log (`dbProvider - NO ENDPOINT ---------------------`);
				// Offline
				if (!config.server) {
					config.server = "localhost" ;
					config.region = "localhost";
				}
				console.log (`dbProvider - createClientConfig - dbProvider - Server=${config.server}`);
				config.endpoint = `http://${config.server}:${config.port}`;

			}

			// Online
			console.log (`dbProvider - createClientConfig - dbProvider - Endpoint=${process.env.AWS_DYNAMO_ENDPOINT}`);
			clientConfig
				=
				{
					apiVersion: '2012-10-08',
					region: config.region ,
					endpoint: config.endpoint ,
					logger: console,
					httpOptions: {      
						timeout: 1500   
					}, 
					maxRetries: 2
				};
			

		}
		else
		{
			// Normal
			console.log (`dbProvider - createClientConfig - dbProvider - Standard`);

			clientConfig
				=
				{
					apiVersion: '2012-10-08',
					region: config.region ,					
					logger: console,
					httpOptions: {      
						timeout: 1000   
					},
					maxRetries: 2
				};
		}
	
		if ( process.env.TRACE ) console.log (`dbProvider - createClientConfig - config`, config);
		if ( process.env.TRACE ) console.log (`dbProvider - createClientConfig - clientConfig `, clientConfig);
		
		return clientConfig ;
	}
	catch(e) {
	  console.error(`dbProvider - createClientConfig Error - ${e.message}`);
	  throw e ;
	}
}

//------ Exports and default -----------------------------------------------------------
//initDefaultConfig();
module.exports = {
		getClientConfig: () => createClientConfig(),
	    getDb: () => new AWS.DynamoDB(createClientConfig()),
		getDocumentClient: () => new AWS.DynamoDB.DocumentClient(createClientConfig()),
		tables: tables,
	    endpoint:  process.env.AWS_DYNAMO_ENDPOINT
	};








