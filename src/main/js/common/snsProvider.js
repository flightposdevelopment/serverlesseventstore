const AWS = require("aws-sdk");
const LOCAL_PORT=4566;

var topicArn ;
console.log (`snsProvider - INIT - env.SNS_ENABLED='${process.env.SNS_ENABLED}'`);
const config = {
    enabled: (process.env.SNS_ENABLED === undefined) ? true :  (String(process.env.SNS_ENABLED).toLowerCase() == "true") ,
	port: 4566 ,
    accountId : process.env.topicAccountId ,
	region: process.env.topicRegion,
    topicName : process.env.topicName 
};
if ( process.env.SNS_SERVER !== undefined ) {
    config.server = process.env.SNS_SERVER ;
    config.region = process.env.SNS_SERVER ;
    console.log (`snsProvider - config.server=${config.server}`);
    }
if ( process.env.SNS_PORT !== undefined ) config.port = process.env.SNS_PORT ;	
console.log (`snsProvider - INIT - enabled=${config.enabled}`);

/* Set up by a call to createClientConfig() */
var snsInstance ;
function getConfig() {
    return config ;
}
function createTopicArn() {

    if ( !getConfig().enabled ) {
        console.log (`snsProvider - createTopicArn - DISABLED`);
        return null ;
    }

    if ( topicArn ) return topicArn ;

    createClientConfig();
    topicArn  = `arn:aws:sns:${config.region}:${config.accountId}:${config.topicName}` ;

    return topicArn ;
}
function createClientConfig() {

    if ( snsInstance ) return snsInstance ;
    if ( !getConfig().enabled ) {
        console.log (`snsProvider - createTopicArn - DISABLED`);
        return null ;
    }

    
	console.log (`snsProvider - createClientConfig`);
    clientConfig = null ;

    if ( process.env.IS_OFFLINE ) {
        console.log (`snsProvider - IS_OFFLINE ---------------------`);

        if (!config.server) {
            console.log (`snsProvider - NO ENDPOINT ---------------------`);
            config.server = "localhost" ;
            config.region = "localhost";
        }
        else
        if (!config.region) {
            config.region = 'us-east-1';
        }
        console.log (`snsProvider - REGION ${config.region} ---------------------`);

        snsInstance = new AWS.SNS({
            endpoint: `http://${config.server}:${config.port}`,
            region: config.region,
          });

        // The ARN and instance id seem to be fixed to 123456789012 & us-east-1
        config.accountId = '123456789012';
        

    }
    else{
        snsInstance = new AWS.SNS();
    }

    return snsInstance ;
}

//------ Exports and default -----------------------------------------------------------
module.exports = {
    getSNS: () => createClientConfig(),
    getTopicArn: () => createTopicArn(),
    getSNSConfig: () => getConfig()
};