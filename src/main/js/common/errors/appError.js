// -----------------------------------------------------------------------------------------
// Custom errors
//
// See: Custom Errors | https://gist.github.com/slavafomin/b164e3e710a6fc9352c934b9073e7216
// See: Http Codes    | https://restfulapi.net/http-status-codes/
//
module.exports = class AppError extends Error {
  constructor (message, status) {
  
	console.error (`ERROR message : ${message}`);
    // Calling parent constructor of base Error class.
    super(message);
    
    // Saving class name in the property of our custom error as a shortcut.
    this.name = this.constructor.name;

    // Capturing stack trace, excluding constructor call from it.
    let captureStatus = false ;
    if (status)
    	{
    	captureStatus = (status===500);
    	}
    if (captureStatus)
    	{
    	// Get it
        Error.captureStackTrace(this, this.constructor);
    	}
    else
    	{
    	// Remove it
        delete this.stack ;
    	}
    
    // You can use any additional properties you want.
    // I'm going to use preferred HTTP status for this error types.
    // `500` is the default value if not specified.
    this.statusCode = status || 500; 
    
  }
};
