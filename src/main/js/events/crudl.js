/* eslint-disable no-console */

const AWS = require('aws-sdk')
const uuid = require('uuid');
const eventProcessor = require('../process/streamHandler');
const uuidV4Regex = /^[a-f\d]{8}-[a-f\d]{4}-4[a-f\d]{3}-[89AB][a-f\d]{3}-[a-f\d]{12}$/i;
const guidRegex = /^[0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12}$/i;
const pseudoRegex = /^[0-9a-f]{32}$/i
const AppError   = require("../common/errors/appError.js");
const dbProvider = require('../common/dbProvider');

const tableName = process.env.tableName ;

function create2(evt, ctx, cb) {
  // item = { id: 5, name: 'Phlebotinum', description: 'desc1', price: 3.99 }
  const item = JSON.parse(evt.body);
  const dynamoCli =dbProvider.getDocumentClient();

  dynamoCli.put(
    {
      Item: item,
      TableName: tableName
    },
    (err, resp) => {
      if (err) {
        cb(err)
      } else {
        cb(null, {
          statusCode: 201,
          headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
          },
          body: JSON.stringify(resp)
        })
      }
    }
  )
}

//----------------------------------------------------------------------------
function addDynamoErrorDetails(sourceFn,errObj){

	errObj.source = { 
				"sourceFn": sourceFn
				//"dynamoCfg" : dynamoCfg 
				};

	return errObj ;
}

//----------------------------------------------------------------------------
// Only works for single value Keys.
// The keyParams need to be of the form...
//{
//	sourceId: [uuid/string] ,
//	createdAt: [time/number]
//}
//
function dynamoGet(keyParams)
{
	return new Promise(function(resolve, reject) 
		{
		console.log(`---- dynamoGet ${keyParams.sourceId} --- `);
		const dynamoCli =dbProvider.getDocumentClient();
		dynamoCli.get(
				    {
				    Key: keyParams,
				    TableName: tableName
				    },
				    (err, data) => 
				    	{
				    		if (err) {
								console.log(`---- dynamoGet ${keyParams.sourceId} ERR --- `);
								addDynamoErrorDetails("Get",err);
					    		console.log(err);
				    			reject(err);
				    		} 
				    		else {
					    		console.log(`---- dynamoGet ${keyParams.sourceId} DATA --- `);
					    		//console.log(data);
				    			resolve(data.Item);
				    		}
				    	}
				    );
		}); // Promise end		  
}

//----------------------------------------------------------------------------
// The 'keyParams' need to be ...
//	{
//	sourceId: [uuid/string] ,
//	createdAt: [time/number]
//	}
//
function dynamoUpdateEventData(keyParams,eventDataUpdate,otherParams)
{
	console.log(`---- dynamoUpdateEventData -- `);
	
	return new Promise(function(resolve, reject) 
		{
		const eventTimestamp = new Date().getTime();
		const params = {
			    Key: keyParams,
			    TableName: tableName,
			    AttributeUpdates: {
			        'event' : {
			            Action: "PUT",
			            Value: eventDataUpdate
			        },
			        'updatedAt' : {
			            Action: "PUT",
			            Value: eventTimestamp
			        }
			    }};
		
		if ( otherParams && isObject(otherParams.inSnapshot) ) 
			{
			console.log(`---- dynamoUpdateEventData -- (inSnapshot)`);
			params.AttributeUpdates.inSnapshot = {
										            Action: "PUT",
										            Value: otherParams.inSnapshot
										        };
			}
		
		
		const dynamoCli =dbProvider.getDocumentClient();
		dynamoCli.update(params,
			    	 (err, data) => 
			    	{
			    		if (err) {
				    		console.log(`---- dynamoUpdateEventData ${keyParams.sourceId} ERR --- `);
							addDynamoErrorDetails("Update",err);
				    		console.log(err);
			    			reject(err);
			    		} 
			    		else {
				    		console.log(`---- dynamoUpdateEventData ${keyParams.sourceId} DATA --- `);
							console.log(data);
							const eventName = eventDataUpdate.eventName ;
							eventProcessor.publish(eventName,keyParams.sourceId,eventTimestamp);
			    			resolve(data.Item);
			    		}
			    	}
			    );		
		
		}); // Promise end		  
}

//----------------------------------------------------------------------------
// Scan does not need any parameters and can return all the data in the db.
// Use with care.
// You can use the Scan operation to retrieve all of the data from a global secondary index. 
// You must provide the base table name and the index name in the request. 
// With a Scan, DynamoDB reads all of the data in the index and returns it to the application. 
// You can also request that only some of the data be returned, and that the remaining data should be discarded. 
// To do this, use the FilterExpression parameter of the Scan operation. 
//
async function dynamoScan(after,before)
{
	console.log(`---- dynamoScan after=${after}, before=${before}  --- `);

	const params  = {
		"TableName": tableName
	  };

	if  ( isObject(after) && isObject(before) )
		{
		// Both defined
		console.log(`---- dynamoScan - BETWEEN  --- `);
		params.FilterExpression = "#created BETWEEN :begin AND :end";
		params.ExpressionAttributeNames = { "#created" : "createdAt" };
		params.ExpressionAttributeValues = { ":begin" : after , ":end" : before };
		}
	else
	if  ( isObject(after) ) {
		console.log(`---- dynamoScan - AFTER  --- `);
		params.FilterExpression = "#created >= :when";
		params.ExpressionAttributeNames = { "#created" : "createdAt" };
		params.ExpressionAttributeValues = { ":when" : after };
	}
	else if  ( isObject(before) ) {
		console.log(`---- dynamoScan - BEFORE  --- `);
		params.FilterExpression = "#created <= :when";
		params.ExpressionAttributeNames = { "#created" : "createdAt" };
		params.ExpressionAttributeValues = { ":when" : before };
	}

	console.log(`---- dynamoQuery params=`,params);

	const dynamoCli =dbProvider.getDocumentClient();

	var returnData = null ;
	let lastKey = null;		
	let rxCount = 0 ;
	do {
		rxCount++ ;
		console.log(`---- dynamoScan PASS ${rxCount} lastKey=${lastKey} ----`);
		
		if ( lastKey ) 
			params.ExclusiveStartKey = lastKey;

		const dynamoDBPromise = new Promise((resolve, reject) => {
			console.log('---- dynamoScan SCAN Params',params);
		    dynamoCli.scan
		            (
					params ,
					(err, data) => 
						{
							console.log(`---- dynamoScan RESULTS! --- `);
							console.log("err",err);
							console.log("data",data);
							if (err) {
								addDynamoErrorDetails("Scan",err);
								err.message = `DYNAMO[${tableName}]:  ${err.message}`;
								reject(err);
							} 
							else {
								console.log(`---- dynamoScan RESULTS=>(count)=`,data.Count);
								console.log(`---- dynamoScan RESULTS>`,data);

								resolve(data);
							}
						}
					);
		  });

		console.log('---- dynamoScan: Wait for promise');
		const data = await dynamoDBPromise ;
		console.log('---- dynamoScan: Data returned', data );

		if ( data == null ) break;
		
		if ( returnData ) {
			returnData.Count += data.Count ;
			returnData.ScannedCount += data.ScannedCount ;
			
			returnData.Items.push(...data.Items);

		} else {
			returnData = data ;
		}
		// Do we continue?
		lastKey = data.LastEvaluatedKey ;
		console.log(`---- dynamoScan lastKey=`,lastKey);
		console.log(`---- dynamoScan found count =`,returnData.Count);
		console.log(`---- dynamoScan found ScannedCount =`,returnData.ScannedCount);
		} 
	while (lastKey);
	
	console.log(`---- dynamoQuery done`);
	return returnData ;
		
}

//----------------------------------------------------------------------------
function dynamoQuery(withId,after,before)
{
	return new Promise(function(resolve, reject) 
		{
		console.log(`---- dynamoQuery withId=${withId}, after=${after}, before=${before}  --- `);
		console.log(`---- dynamoQuery after=${isObject(after)}, before=${isObject(before)}  --- `);
		
		let query  = null ;
		let attr   = { "#id": "sourceId" };
		let values = { ":uuid": withId };		

		if  ( isObject(after) && isObject(before) )
			{
			// Both defined
			console.log(`---- dynamoQuery - BETWEEN  --- `);
			query = "#id = :uuid and #created BETWEEN :begin AND :end";
			attr["#created"] = "createdAt" ;
			values[":begin"] = after ;
			values[":end"]   = before ;
			}
		else
		if  ( isObject(before) )
			{
			// Only before defined
			console.log(`---- dynamoQuery - BEFORE  --- `);
			query = "#id = :uuid and #created <= :when";
			attr["#created"] = "createdAt" ;
			values[":when"] = before ;
			}
		else
			{
			// Both undefined or only after defined
			if  ( !isObject(after) ) 
				// Use a beginning of time option.
				after = 0 ;

			console.log(`---- dynamoQuery - AFTER  --- `);
			query = "#id = :uuid and #created >= :when";
			attr["#created"] = "createdAt" ;
			values[":when"] = after ;
			}
		
		// Perform the query
		console.log(`---- dynamoQuery query=[${query}] `);
		console.log(`---- dynamoQuery attr=`,attr );
		console.log(`---- dynamoQuery values=`, values );

		const dynamoCli =dbProvider.getDocumentClient();
		dynamoCli.query(
				    {
				      TableName: tableName ,
				      KeyConditionExpression: query,
				      ExpressionAttributeNames: attr,
				      ExpressionAttributeValues: values  
				    },
				    (err, data) => 
				    	{
				    		if (err) {
								console.log(`---- dynamoQuery - ERR --- `);
								addDynamoErrorDetails("Query",err);
					    		console.log(err);
				    			reject(err);
				    		} 
				    		else {
					    		console.log(`---- dynamoQuery - OK --- `);
					    		console.log(data);
				    			resolve(data);
				    		}
				    	}
				    );
		
		}); // Promise end		  
}

//----------------------------------------------------------------------------
// @param item Is the actual event data.
//
function dynamoPut(vId,item,eventName)
{
	console.log("--- dynamoPut");		
	return new Promise(function(resolve, reject) 
		{
		console.log("--- dynamoPut - promise");		
		const eventTimestamp = new Date().getTime();
		const eventData = {
				sourceId : (vId ? vId : uuid.v4()),
				eventName : eventName ,
				event : item ,
				inSnapshot: false,
	            createdAt: eventTimestamp,
			    snapshotAt: eventTimestamp,
			    updatedAt: eventTimestamp
		};
		
		console.log("--- dynamoPut:",eventData);		
		const dynamoCli =dbProvider.getDocumentClient();
		dynamoCli.put(
				    {
			    	Item: eventData ,
			    	TableName: tableName
				    })
			.promise()
			.then(function(data) {
			    console.log(data);
			    
			    console.log("--- dynamoPut: OK - Event publish ");
								
				const eventName = eventData.eventName ,
					sourceId  = eventData.sourceId ,
					createdAt  = eventTimestamp ;  
				
				return eventProcessor.publish(eventName,sourceId,createdAt,item);

			    
			  })
		   .then(function(data) {
		   	
		   		console.log("--- dynamoPut: OK");
    			resolve(eventData);
		   	
		   })
		   .catch(function(err) {
				console.log("--- dynamoPut: err=",err);	
				addDynamoErrorDetails("Put",err);

				err.message = "dynamoPut ERROR, cause " + err.message ;

				reject(err);
		    });
		}); // Promise end		  
}

//----------------------------------------------------------------------------
function buildResponse(responseBodyContent,responseStatus)
{
    responseStatus = responseStatus ? responseStatus : 200 ;

	console.log(`buildResponse - responseStatus=${responseStatus}`);

	const responseHeaders = {
		"Content-Type": "application/json",
		"X-Content-Type": "application/json",
		'Access-Control-Allow-Origin': '*', // Required for CORS support to work
	    'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
		}; 

    const responseBody = responseBodyContent ;
    responseBody._dbg   = {
			"tableName": tableName,
			"config": dbProvider.getClientConfig()
    	};
    if ( responseStatus != 200 ) responseBody.status = responseStatus ;
    
	// Build the response.
    const response = {
        "statusCode": responseStatus,
        "headers": responseHeaders,
        "body": JSON.stringify(responseBody),
        "isBase64Encoded": false, // Not binary data
    	};
    
    return response;
}

//----------------------------------------------------------------------------
function paramId(evt)
{
	return evt.pathParameters
	     ? String(evt.pathParameters.id).toLowerCase()
	     : null ;
}
//----------------------------------------------------------------------------
function paramAt(evt)
{
	return evt.pathParameters
	     ? evt.pathParameters.at
	     : null ;
}
//----------------------------------------------------------------------------
function paramEventName(evt,defaultEventName)
{
	console.log(`---- paramEventName --- `,evt.pathParameters);
	
	return evt.pathParameters
	     ? evt.pathParameters.eventName.toUpperCase()
	     : defaultEventName ;
}
//----------------------------------------------------------------------------
function validateId(id) {
	return guidRegex.test(id) ||  uuidV4Regex.test(id) || pseudoRegex.test(id);
}

//----------------------------------------------------------------------------
function parseId(evt)
{
	return new Promise(function(resolve, reject) 
			{
			var id = paramId(evt);
			
			if ( id )
				{
				console.log(`---- parseId --- ${id}`);
				
				id = id.toLowerCase(); // Because UUIDs are lowercase

				if ( validateId(id) )
					resolve(id);
				else
					reject(new Error(`'id' param '${id}' is not a UUID`));				
				}
			else
				{
				resolve(null);
				}
			
			}); // Promise end	
}
//----------------------------------------------------------------------------
// returns a promise for the search params on a get.
// Returns an object of the type ...
// {
// 	sourceId: [uuid/string] ,
//	createdAt: [time/number]
// }
//
function parseGetParams(evt)
{
	return new Promise(function(resolve, reject) 
			{
			let err = null ;
			const id = paramId(evt);
			const at = paramAt(evt);
			
			if ( id )
				{
				if ( validateId(id) ) 
					{
					console.log(`---- parseGetParams --- id:${id}`);
					}
				else
					{
					console.log(`---- parseGetParams --- id:${id} -- is not a UUID`);
					err = new Error(`'id' param '${id}' is not a UUID`);				
					}
				}
			else
				err = new Error(`'${id}' is not defined`);	

			if ( at && !err )
				{
				const nan = isNaN(at);
				
				if ( nan ) 
					{
					console.log(`---- parseGetParams --- at:${at} -- not a number`);
					err = new Error(`'at' param '${at}' is not a Number`);				
					}
				else
					{
					console.log(`---- parseGetParams --- at:${at}`);
					}
				}
			else
				err = new Error(`'${at}' is not defined`);	

			
			
			if ( err )
				reject(err);				
			else
				resolve({
					sourceId: id ,
					createdAt: parseInt(at,10)
				});
			
			}); // Promise end	
}


//----------------------------------------------------------------------------
//Returned object is never empty as empty objects are rejected.
function parseUpdateRequestBody(searchParams,rqEvt)
{
	console.log(`---- parseUpdateRequestBody`);
	return new Promise((resolve, reject) =>
			{
			const item = JSON.parse(rqEvt.body);
			console.log(`---- parseUpdateRequestBody -- parsed `);
			
			if (isEmptyObject(item))
				{
				reject(new AppError("Empty Event Object",400));
				}
			else
				{
				resolve({"searchParams":searchParams,"item":item});										
				}				
			})
			.catch(ex=>{ throw (new AppError(`Parsing body: ${ex.message}`,400));})
	; // Promise end	
}

//----------------------------------------------------------------------------
// Returned object is never empty as empty objects are rejected.
function parsePutRequestBody(sourceId,rqEvt)
{
	return new Promise(function(resolve, reject) 
			{
			const item = JSON.parse(rqEvt.body);
			
			if (isEmptyObject(item))
				{
				reject(new Error("Empty Event Object"));
				}
			else
				{
				const vId = (sourceId ? sourceId : uuid.v4());
				
				resolve({"vId":vId,"item":item});										
				}
			}); // Promise end	
}
//----------------------------------------------------------------------------
//This should work in node.js and other ES5 compliant implementations.
function isEmptyObject(obj) {
  return isObject(obj) && !Object.keys(obj).length;
}
//This should work in node.js and other ES5 compliant implementations.
function isObject(obj) {
  return obj && (typeof obj !== 'undefined');
}
//----------------------------------------------------------------------------
function parseQuery(evt)
{
	return new Promise(function(resolve, reject) 
			{
			console.log(`---- parseQuery --- `);

			let params = {};
			const id = paramId(evt);
			
			if ( evt.queryStringParameters && evt.queryStringParameters.after ) 	params.after  = parseInt(evt.queryStringParameters.after, 10);
			if ( evt.queryStringParameters && evt.queryStringParameters.before ) 	params.before = parseInt(evt.queryStringParameters.before, 10);			
			if ( validateId(id) ) 								 					params.withId = id ;
				
				
			console.log(`---- parseQuery after=${params.after} `);
			console.log(`---- parseQuery before=${params.before} `);
			console.log(`---- parseQuery withId=${params.withId} `);
			
			if ( isEmptyObject(params) )
				{
				console.log(`---- parseQuery NO DATA `);
				resolve(null);
				}
			else
				{
				resolve(params);
				}
			
			}); // Promise end	
}
//----------------------------------------------------------------------------
function addEventName(eventObj,eventName)
{
	console.log(`---- addEventName: ${eventName} ---`,eventObj);
	
	if ( !isObject(eventObj.item) )
		{
		// this is a problem
		throw new Error(`No event object when adding event:${eventName}'.`);
		}
	else
	if ( isObject(eventObj.item.eventName) )
		{
		// this is a problem
		throw new Error(`Event has name '${eventObj.item.eventName}' already defined when defining as '${eventName}'.`);
		}
	else
		{
		eventObj.eventName = eventName ;
		}
	
	return eventObj ;
}
//----------------------------------------------------------------------------
async function create(evt, ctx) {
	
	console.log(`---- CREATE --- ${tableName}`);
	//const item = JSON.parse(evt.body);
	const rs = 
		await
		parseId(evt)
		.then(vId => parsePutRequestBody(vId,evt))	//  Parsed object is never empty as empty objects are rejected.
		.then(obj => addEventName(obj,paramEventName(evt,"NEW")))
		.then(obj => dynamoPut( obj.vId,
								obj.item,
								obj.eventName))
		.then(item => {
			const rs = buildResponse({"item":item});
			return rs ;			
		})
		.catch(function(error) {
			const errRs = buildResponse({
											error:error.message,
											source:error.source
										},
										error.statusCode ? error.statusCode : 500);
			console.log(`---- Create --- ERR ${errRs}`);
			console.log(error);
			return errRs ;
		});

	return rs ;

}


//----------------------------------------------------------------------------
// Get a single by Id value
async function read(evt, ctx, cb) {
	
	console.log(`---- READ1 --- ${tableName}`);
	
	
	const rs = 
		await
		parseGetParams(evt)
		.then(params => dynamoGet(params))
		.then(item => {

  		console.log(`---- READ1 -- request get DATA --- `);
  		console.log(item);

			
			if ( item && item.sourceId )
				{
				return buildResponse({"event":item});
				}
			else
				{
				return buildResponse({ sourceId: paramId(evt) , error : 'Not Found' },404);
				}
				
		})
		.catch(function(error) {
			const errRs = buildResponse({error:error.message},error.statusCode ? error.statusCode : 500);
			console.log(`---- READ1 --- ERR ${errRs}`);
			return errRs ;
		});

	console.log(`---- READ1 --- DONE`);
	return rs ;	
}

//----------------------------------------------------------------------------
//Get a single by Id value
async function read(evt, ctx, cb) {
	
	console.log(`---- READ --- ${tableName}`);
		
	const rs = 
		await
		parseGetParams(evt)
		.then(params => dynamoGet(params))
		.then(item => {

			console.log(`---- READ -- request get DATA --- `);
			//console.log(item);

			if ( item && item.sourceId )
				{
				return buildResponse({
									"scannedCount":1,	// Not really true
									"eventCount":1,
									"events": [ item ]
									});
				}
			else
				{
				return buildResponse({ sourceId: paramId(evt) , error : 'Not Found' },404);
				}
	
		})
		.catch(function(error) {
			const errRs = buildResponse({error:error.message},error.statusCode ? error.statusCode : 500);
			console.log(`---- READ --- ERR ${errRs}`);
			return errRs ;
		});

	console.log(`---- READ --- DONE`);
	return rs ;	
}


//----------------------------------------------------------------------------
//Get a single by Id value
async function update(evt, ctx, cb) {
	
	console.log(`---- UPDATE --- ${tableName}`);
		
	const rs = 
		await
		parseGetParams(evt)
		.then(params => parseUpdateRequestBody(params,evt))	//  Parsed object is never empty as empty objects are rejected.
		.then(params => dynamoUpdateEventData(params))
		.then(item => {

		console.log(`---- UPDATE get DATA --- `);
		console.log(item);

			
			if ( item && item.sourceId )
				{
				return buildResponse({"event":item});
				}
			else
				{
				return buildResponse({ sourceId: paramId(evt) , error : 'Not Found' },404);
				}

		
		})
		.catch(function(error) {
			console.log(`---- UPDATE --- ERR ${error.message} ${error.statusCode}`);
			const errRs = buildResponse({error:error.message},error.statusCode ? error.statusCode : 500);
			console.log(`---- UPDATE --- ERR ${errRs}`);
			return errRs ;
		});

	console.log(`---- UPDATE --- DONE`);
	return rs ;	
}


//----------------------------------------------------------------------------
async function list(evt, ctx, callback) {
	
	console.log(`---- Listing --- ${tableName}`);
	
	const rs = 
		await
		parseQuery(evt)
		.then(params => {
			console.log(`params`,params);

			if (isObject(params)) {
				if ( isObject(params.withId) )
					return dynamoQuery( params.withId,
						params.after ,
						params.before);
				else
					return dynamoScan(params.after ,
									  params.before); 
			}
			else
				return dynamoScan(); 
		})
		.then(data => {

			data.Items.forEach(element => { 

				var ref = `/${process.env.STAGE}/${evt.path}`;
				ref = ref.replace('/global',`/${element.sourceId}`);
				ref = ref.replace('{id}',`${element.sourceId}`);
				ref = ref.replace('//','/');

				element.links = {
					"self": {
						"href" : ref 
						}
					};

			//console.log(`element`,element);
			});

			const rs = buildResponse({
								"scannedCount": data.ScannedCount,
								"eventCount": data.Count,
								"events":data.Items});
			return rs ;			
		})
		.catch(function(error) {
			const errRs = buildResponse({error:error.message},error.statusCode ? error.statusCode : 500);
			console.log(`---- Listing --- ERR:`,error);
			return errRs ;
		});

	return rs ;

}

module.exports = { create, read, update, list , buildResponse , dynamoPut, dynamoUpdateEventData , paramId , isEmptyObject };
