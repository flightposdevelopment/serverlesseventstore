var fs = require('fs');
const path = require('path');

//
// Process Lambda call
// - Standard for Lambda processing
//- See https://serverless.com/framework/docs/providers/aws/events/apigateway#lambda-integration
//
module.exports.main = function main (event, context, callback) {

	if (process.env.SWAGGER_SOURCE==null) 		{ console.error('Missing the required "SWAGGER_SOURCE" ENVIROMENT VAR.'); return ; }

    // Prepare return data
    // Lambda proxy response
    // See https://aws.amazon.com/premiumsupport/knowledge-center/malformed-502-api-gateway/   
    const responseHeaders = (process.env.SWAGGER_SOURCE.endsWith('.yml')) 
    					  ? {"Content-Type": "text/yaml" }
    					  : {"Content-Type": "application/json" };    
	const filePath = path.join(__dirname, process.env.SWAGGER_SOURCE );
	let responseBody = fs.readFileSync(filePath, 'utf8');

	responseBody = 
				 `# Generated Swagger for https://${event.headers.Host}/${event.requestContext.stage},${event.requestContext.resourcePath}\n` 
				 + responseBody 
				 + 'servers:\n'
				 + `  - url: https://${event.headers.Host}/${event.requestContext.stage}\n`;
	
    const response = {
        "statusCode": 200,
        "headers": responseHeaders,
        "body": responseBody,
        "isBase64Encoded": false // Not binary data
    };
    
    // Return on Lambda
    callback(null, response);  
    };

