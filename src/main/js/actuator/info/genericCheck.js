
//
// Process Lambda call
// - Standard for Lambda processing
//- See https://serverless.com/framework/docs/providers/aws/events/apigateway#lambda-integration
//
module.exports.main = function main (event, context, callback) {

    // Prepare return data
	if (process.env.APP_VERSION==null) 		{ console.error('Missing the required "APP_VERSION" ENVIRONMENT VAR.'); return ; }
	if (process.env.APP_DESCRIPTION==null) 	{ console.error('Missing the required "APP_DESCRIPTION" ENVIRONMENT VAR.'); return ; }
	if (process.env.APP_TITLE==null) 		{ console.error('Missing the required "APP_TITLE" ENVIRONMENT VAR.'); return ; }
	if (process.env.APP_STAGE==null) 		{ console.error('Missing the required "APP_STAGE" ENVIRONMENT VAR.'); return ; }

    // Lambda proxy response
    // See https://aws.amazon.com/premiumsupport/knowledge-center/malformed-502-api-gateway/   
    const responseHeaders = {"Content-Type": "application/x-flightpos.information-v1+json" };
    const responseBody = {
    		  "app": {
    			    "description": process.env.APP_DESCRIPTION,
    			    "version": `v${process.env.APP_VERSION}-${process.env.APP_BUILD}`, 
    			    "name": process.env.APP_TITLE,
                    "stage": `${process.env.APP_STAGE}`
    			  },
    			  "auth": { "href": "[auth_url]" } ,
    			  "host": event.headers.Host ,
    			  "stage": event.requestContext.stage , 
       			  "resourcePath": event.requestContext.resourcePath 
    			} ;
    const response = {
        "statusCode": 200,
        "headers": responseHeaders,
        "body": JSON.stringify(responseBody),
        "isBase64Encoded": false // Not binary data
    };
    
    // Return on Lambda
    callback(null, response);  
    };

