const dbProvider  = require('../../common/dbProvider');
const snsProvider = require('../../common/snsProvider');



//----------------------------------------------------------------------------
function buildResponse(responseBodyContent,responseStatus)
{
    responseStatus = responseStatus ? responseStatus : 200 ;
    const responseHeaders = {"Content-Type": "application/json"}; 
    let responseBody = {};

    
    if ( responseBodyContent.error )
    	{
    	responseBody.error = responseBodyContent.error ;
    	}
    else
    	{
    	responseBody = responseBodyContent ;
    	responseBody.app = {
			"description": process.env.APP_DESCRIPTION,
			"version": `v${process.env.APP_VERSION}-${process.env.APP_BUILD}`, 
			"name": process.env.APP_TITLE,
			"stage": process.env.APP_STAGE
			};
		if ( snsProvider.getSNSConfig().enabled )
			responseBody.sns = 
					{
					"topicArn" : snsProvider.getTopicArn(),
					"config"   : snsProvider.getSNSConfig()
					};
		else
			responseBody.sns = 
					{
					"config"   : { "enabled" : false }
					};
      	}
    
	// Build the response.
    const response = {
        "statusCode": responseStatus,
        "headers": responseHeaders,
        "body": JSON.stringify(responseBody),
        "isBase64Encoded": false, // Not binary data
    	};
    
    return response;
}    
    
    
//----------------------------------------------------------------------------
async function main(evt, ctx, callback) {
	
	console.log(`---- Info ---`);
	
	const responseBody = {
		"nodejs" : process.version,
		"dynamo" : {
			config: dbProvider.getClientConfig(),
			tables: dbProvider.tables
		}
		}; 
	
	const rs = buildResponse(responseBody);
	return rs ;	

}    


module.exports = { main }