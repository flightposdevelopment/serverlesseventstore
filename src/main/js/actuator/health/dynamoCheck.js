/* eslint-disable no-console */
const AWS = require('aws-sdk')
const dbProvider = require('../../common/dbProvider');
const dynamoDB = dbProvider.getDb();


//----------------------------------------------------------------------------
function dynamoCheckTable()
{
	return new Promise(function(resolve, reject) 
		{
		// Describe the dynamo table
		dynamoDB.describeTable(
				    {
			    	"TableName": dbProvider.tables.events
				    },
				    (err, data) => 
				    	{
				    		if (err) {
				    			reject(err);
				    		} 
				    		else {
				    			resolve({
				    					"name": dbProvider.tables.events ,
				    					"status" : data.Table.TableStatus ,
				    					"itemCount" : data.Table.ItemCount ,
				    					"sizeBytes" : data.Table.TableSizeBytes ,
				    					"arn" : data.Table.TableArn ,
				    					//"data":data
				    					});
				    		}
				    	}
				    );
		}); // Promise end		  
}    
//----------------------------------------------------------------------------
function dynamoListTables()
{
	return new Promise(function(resolve, reject) 
		{
		dynamoDB.listTables(
				    {
				    },
				    (err, data) => 
				    	{
				    		if (err) {
				    			reject(err);
				    		} 
				    		else {
				    			resolve(data);
				    		}
				    	}
				    );
		}); // Promise end		  
}    

//----------------------------------------------------------------------------
function buildResponse(responseBodyContent,responseStatus)
{
    responseStatus = responseStatus ? responseStatus : 200 ;
    const responseHeaders = {"Content-Type": "application/json"}; 
    let responseBody = {
			dynamo: {
				config: dbProvider.getClientConfig(),
				tables: {
					events: dbProvider.tables.events
					},
				responseStatus: responseStatus
			}
	};

    if ( responseBodyContent.error )
    	{
    	responseBody.error = responseBodyContent.error ;
    	}
    else
    	{
    	responseBody = responseBodyContent ;
    	}

	if (responseStatus==200)
		responseBody.status = "UP" ;
	else
	if (responseStatus==400)
		responseBody.status = "UNHEALTHY" ;
	else
	if ( responseBody.error == "Cannot do operations on a non-existent table" )
		responseBody.status = "UNHEALTHY" ;
	else 
		responseBody.status = "DOWN"; 

    
	// Build the response.
    const response = {
        "statusCode": responseStatus,
        "headers": responseHeaders,
        "body": JSON.stringify(responseBody),
        "isBase64Encoded": false, // Not binary data
    	};
    
    return response;
}

//----------------------------------------------------------------------------
async function info(evt, ctx, callback) {
	
	console.log(`---- Info --- ${dbProvider.tables.events}`);
	
	const responseBody = {
		"tableName": dbProvider.tables.events ? dbProvider.tables.events : "???"
	};

	return Promise.resolve(responseBody) ;

}    


//----------------------------------------------------------------------------
async function health(evt, ctx, callback) {
	
	console.log(`---- Health --- ${dbProvider.tables.events}`);
	
	const rs = 
		await
		dynamoCheckTable()
		.then(data => {
			
			const responseBody = {
				"status": (data) ? "UP" :"DOWN"	,
				"dynamo" : {
						"status": (data) ? "UP" :"DOWN"	,
						"tableName": dbProvider.tables.events ? dbProvider.tables.events : "???",
						"config": dbProvider.getClientConfig()
						} 
				};
			
			const rs = buildResponse(responseBody);
			return rs ;			
		})
		.catch(function(error) {
			const errRs = buildResponse({error:error.message},500);
			console.log(`---- Health --- ERR `,error.message);
			if (process.env.DEBUG) console.log(error);
			return errRs ;
		});

	return rs ;

}    


module.exports = { info , health , dynamoCheckTable  }