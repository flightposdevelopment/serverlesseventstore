'use strict';
var AWS = require("aws-sdk");
const tableName = process.env.tableName ;
const snsProvider = require('../common/snsProvider');


function publish(eventName,sourceId,createdAt,eventData) {

    const snsConfig = snsProvider.getSNSConfig();
  
    console.log(`SNS-Publish - Config `, snsConfig );

    if ( !snsConfig.enabled ) {
        console.log(`SNS-Publish - SKIP`);
        return ;        
    }
    
    const sns = snsProvider.getSNS();
    const topicArn  = snsProvider.getTopicArn();
    console.log(`SNS-Publish to ${topicArn}`);
    
    var messageJson = {
        links : {}
    };

    if ( sourceId )  messageJson.sourceId = sourceId ;
    if ( eventName ) messageJson.eventName = eventName ;
    if ( createdAt ) messageJson.createdAt = createdAt ;
    if ( eventData ) messageJson.event = eventData ;

    messageJson.links.all = `/events/history/${sourceId}`;
    messageJson.links.self = `/events/history/${sourceId}/${createdAt}`;
    messageJson.links.after = `/events/history/${sourceId}?after=${createdAt}`;
    messageJson.links.before = `/events/history/${sourceId}?before=${createdAt}`;
   
    var msgText = JSON.stringify(messageJson);
    console.log("-- EVENT== ",msgText);
    
    var params = {
        Subject: `eventstore::${eventName}`,
        Message: msgText,
        //MessageStructure: "json",
        TopicArn: topicArn
        };

					
    return sns.publish(params).promise();
    

}

//
// Process Lambda call - Send an event to SNS
//
//----------------------------------------------------------------------------
async function main(evt, ctx,callback) {

    const sns = snsProvider.getSNS();
    const topicArn  = snsProvider.getTopicArn();
    const snsConfig = snsProvider.getSNSConfig();
  
    if ( !snsConfig.enabled ) {
        console.log(`---- process- --- ${tableName} Stream for ${evt.Records.length} records. - Notify on '${topicArn}'`);
        callback(null, `SNS disabled skip ${evt.Records.length} records.`);
        return ;        
    }

	console.log(`---- process- --- ${tableName} Stream for ${evt.Records.length} records. - Notify on '${topicArn}'`);
	console.log('CTX ',`${ctx.functionName}`);
	
    evt.Records.forEach((record) => {
        console.log('Stream record: ', JSON.stringify(record, null, 2));
        console.log('dynamodb: ', JSON.stringify(record.dynamodb, null, 2));
        console.log('dynamodb.Keys: ', JSON.stringify(record.dynamodb.Keys, null, 2));
        console.log('dynamodb.Keys.sourceId: ', JSON.stringify(record.dynamodb.Keys.sourceId, null, 2));
        console.log('dynamodb.Keys.sourceId.S: ', JSON.stringify(record.dynamodb.Keys.sourceId.S, null, 2));
        console.log('dynamodb.NewImage: ', JSON.stringify(record.dynamodb.NewImage, null, 2));
        
        if (record.eventName == 'INSERT') {
            
            const eventName = record.dynamodb.NewImage.eventName.S ,
                  sourceId  = record.dynamodb.Keys.sourceId.S ,
                  createdAt  = record.dynamodb.Keys.createdAt.N  
                  ;

            publish(eventName,sourceId,createdAt);
        }
    });
	
	
	console.log(`----  process- --- DONE`);
	
	callback(null, `Successfully processed ${evt.Records.length} records.`);
}

module.exports = { main , publish }