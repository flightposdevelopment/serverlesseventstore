const jsonata = require("jsonata");
const eventHandler = require("../events/crudl.js");
const AppError = require("../common/errors/appError.js");
const tableName = process.env.tableName ;

//----------------------------------------------------------------------------
// Case sensitivity hack.
function getHeader(evt,header)
{
	const possibles = [
		header,
		header.toLowerCase() 
	];
	
	for(let i = 0; i < possibles.length; i++)
		{
		let headerVal = evt.headers[possibles[i]];
		console.log(`headerVal=${headerVal}`);
		if ( headerVal ) return headerVal ;
		}
	
	return null ;
}
//----------------------------------------------------------------------------
function parseHeaders(evt)
{
	return new Promise(function(resolve, reject) 
			{
			console.log(evt);
			const typeRegex   = /^text\/plain/
			const contentType = getHeader(evt,'Content-Type');
			
			if ( !contentType )
				{
				console.log(`---- parseHeaders --- 415`);
				reject(new AppError(`Cannot process no body content as JSONATA expression.`,415));
				}
			else
			if ( contentType.match(typeRegex) )
				{
				resolve(null);
				}
			else
				{
				console.log(`---- parseHeaders --- 415`);
				reject(new AppError(`Cannot process type of [${contentType}]`,415));
				}
			
			}); // Promise end	
}
//----------------------------------------------------------------------------
function parseBody(evt)
{
	return new Promise(function(resolve, reject) 
			{
			const bodyContent   = evt.body ;
			
			if ( !bodyContent )
				{
				reject(new AppError(`No Body posted`,422));
				}
			else
			if ( bodyContent.trim() === '' )
				{
				reject(new AppError(`No Body content posted`,422));
				}
			else
				{
				const expText = bodyContent.trim();
				console.log(`---- parseBody --- "${expText}"`);
			
				const expression = jsonata(expText);
				console.log(`---- parseBody --- DONE`);
				resolve(expression);				
				}
			
			}); // Promise end	
}


//
//Process Lambda call
//
//----------------------------------------------------------------------------
async function fetchList(evt, ctx) {
	
	console.log(`---- Process-List --- ${tableName}`);
	
	let expression = null ;
	const rs = 
		await
		parseHeaders(evt)
		.then(data => parseBody(evt))
		.then(jsonataExp  => {
			expression = jsonataExp;
			return eventHandler.list(evt, ctx);			
		})
		.then(listRs  => {
			console.log(`>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ${listRs.statusCode}`);

			if ( listRs.statusCode == 200 )
				{
				console.log(`---- Process-List --- Run Expression on ...`);
				console.log(listRs.body);
				const data = JSON.parse(listRs.body) ;
				console.log(data);
				const processed = expression.evaluate(data);
				console.log(processed);
				if ( processed )
					return { "out":processed,
							 "src" :data      };							
				else
					throw new AppError (`No data returned by expression ${evt.body}`,404);					
				}
			else
				{
				console.log(`---- Process-List --- ${listRs.statusCode} fall through.`);
				return listRs ;	
				}
		})	
		.catch(function(error) {
			
			if ( error.token && error.position )
				return eventHandler.buildResponse({error:`JSONATA error at ${error.position}, due to '${error.token}'`,
											 syntaxErr:error.message },
											 error.statusCode || 406);
			else
				return eventHandler.buildResponse({error:error.message},error.statusCode || 500);
		});

	console.log(`----  Process-List --- DONE`);
	
	return rs ;
	
}

//
// Process Lambda call
//
//----------------------------------------------------------------------------
async function list(evt, ctx) {
	
	console.log(`---- Process-List --- ${tableName}`);
	
	let expression = null ;
	const rs = 
		await
		fetchList(evt, ctx)
		.then(listRs  => {

			if ( listRs.src &&  listRs.out )
				{
				// Not the processed call, repack & throw away the source.
				return eventHandler.buildResponse({"data":listRs.out});							
				}
			else
				{
				console.log(`---- Process-List --- ${listRs.statusCode} fall through.`);
				return listRs ;	
				}
		});

	console.log(`----  Process-List --- DONE`);
	
	return rs ;
	
}

module.exports = { list , fetchList }