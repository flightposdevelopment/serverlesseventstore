const jsonataHandler = require("./jsonataHandler.js");
const tableName = process.env.tableName ;
const eventHandler = require("../events/crudl.js");

//
// Process Lambda call
//
//----------------------------------------------------------------------------
async function list(evt, ctx) {
	
	console.log(`---- Snapshot-List --- ${tableName}`);
	
	let expression = null ;
	const rs = await 
		jsonataHandler
			.fetchList(evt,ctx)
			.then(listRs  => {

				// Only snapshot if there is data
				if ( listRs.src && listRs.out )
					{
					if ( eventHandler.isEmptyObject(listRs.out) )
						{
						return eventHandler.buildResponse({error:`Snapshot not created JSONATA expression did not return data.`},404);							
						}
					else
						{
						// Not the processed call, repack & throw away the source.
						
						listRs.src.events.forEach(function(eventHolder) {
							  console.log("---------------------------------------------------------------");
							  console.log(eventHolder.sourceId);
							  console.log(eventHolder.createdAt);
							  console.log(eventHolder.updatedAt);
							  
							  eventHandler.dynamoUpdateEventData( { sourceId:eventHolder.sourceId, createdAt: eventHolder.createdAt }, 
										  						  eventHolder.event ,
										  						  { "inSnapshot": true }
							  									  );	

							  console.log("---------------------------------------------------------------");
							});
						
						eventHandler.dynamoPut( eventHandler.paramId(evt),
												listRs.out, // The direct output
											    "SNAPSHOT");
						
						
						return eventHandler.buildResponse(listRs);							
						}
					}
				else
					{
					console.log(`---- Snapshot-List --- ${listRs.statusCode} fall through.`);
					return listRs ;	
					}
			});				
			
	
	console.log(`----  Snapshot-List --- DONE`);
	
	return rs ;
	
}

module.exports = { list }