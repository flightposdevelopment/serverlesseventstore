'use strict';
const snsProvider = require('../common/snsProvider');
//var AWS = require("aws-sdk");
//const PORT=4575;
//const TOPIC= 'demo';
// The ARN and instance id seem to be fixed to 123456789012 & us-east-1
//const REGION= 'us-east-1';
//const INSTANCE = '123456789012';


module.exports.ping = (event, context, callback) => {
  var sns = snsProvider.getSNS();
  var topic = snsProvider.getTopicArn() ;

  console.log("-------- HTTP ------ ping Start on event:" + topic );

  sns.publish({
    Message: '{"default": "hello!"}',
    MessageStructure: "json",
    TopicArn: topic ,
  }, () => {
    console.log("-------- HTTP ------ ping OK");
    //callback(null, {response: "return from lambda ping"});

    const responseBody = { "ping" : "ok" ,
        "message" : `Pinged ${topic}`
    };
    
    const response = {
        "statusCode": 200,
        "body": JSON.stringify(responseBody),
        "isBase64Encoded": false // Not binary data
    };
    
    // Return on Lambda
    callback(null, response);  
    
    
  });
  
};

module.exports.pong = (event, context, callback) => {
  console.log("-------- SNS ------ pong");
  console.log(JSON.stringify(event, null, "\t"));
  //console.log(event.Records[0].Sns.Message);

  var obj = JSON.parse(event.Records[0].Sns.Message);
  console.log(JSON.stringify(obj, null, "\t"));

  callback(null, {response: "return from lambda pong"});
};
