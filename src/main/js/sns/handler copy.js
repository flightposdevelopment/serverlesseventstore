'use strict';
var AWS = require("aws-sdk");

const PORT=4575;
const TOPIC= 'demo';
// The ARN and instance id seem to be fixed to 123456789012 & us-east-1
const REGION= 'us-east-1';
const INSTANCE = '123456789012';


module.exports.ping = (event, context, callback) => {
  var sns = new AWS.SNS({
    endpoint: `http://127.0.0.1:${PORT}`,
    region: REGION,
  });
  sns.publish({
    Message: '{"default": "hello!"}',
    MessageStructure: "json",
    TopicArn: `arn:aws:sns:${REGION}:${INSTANCE}:${TOPIC}`,
  }, () => {
    console.log("-------- HTTP ------ ping");
    callback(null, {response: "return from lambda ping"});
  });
};

module.exports.pong = (event, context, callback) => {
  console.log("-------- SNS ------ pong");
  console.log(JSON.stringify(event));
  // console.log(event.Records[0].Sns.Message);
  callback(null, {response: "return from lambda pong"});
};
