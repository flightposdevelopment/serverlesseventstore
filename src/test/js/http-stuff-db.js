const fs = require('fs');
const myhttp = require('./http.js');
const http = new myhttp()
					.baseUrl('http://localhost:3000')

async function testAddCar()
{
	console.log("testPost");
	const contents = fs.readFileSync("src/test/resources/cars/jag.json", 'utf8');
	console.log(contents);
	
	const rqData = JSON.parse(contents);
	
	const rsData =
		await http
			.post('/events/add/new',rqData)
			.then(result => {
			        userDetails = result;
			        console.log("testPost:OK ---- ");
			        // Use user details from here
			        console.log(userDetails)
			        return userDetails ;
			    })
		    .catch(err => {
		    	console.log("testPost:FAIL ---- ");
		        console.log(err);
		    });

	console.log(`testPost:RS id=${rsData.item.sourceId} ---- `);
}

async function testFindCar()
{
//	console.log("testPost");
//	const contents = fs.readFileSync("src/test/resources/cars/jag.json", 'utf8');
//	console.log(contents);
	
	// First piece of test data
	// Matching the NEW record & with the correct Licence Number
	const rqData = "(events[eventName='NEW' and event.Dvla.VRM='LF55HDC'].sourceId)[0]";
	
	const rsData =
		await http
			.put('/events/history/global',rqData,'text/plain')
			.then(result => {
			        console.log("testPost:OK ---- ");
			        jsonResult = JSON.parse(result);
			        console.log(jsonResult)
			        return jsonResult ;
			    })
		    .catch(err => {
		    	console.log("testPost:FAIL ---- ");
		    });


	if (rsData)
		{
		console.log(`Data ---- `);
		console.log(rsData.data);
		
		}
//		console.log(`testPost:RS id=${rsData.item.sourceId} ---- `);

}

async function testAddKeeper()
{
	console.log("testPost");
	const contents = fs.readFileSync("src/test/resources/cars/jag.json", 'utf8');
	console.log(contents);
	
	// First piece of test data
	// Matching the NEW record & with the correct Licence Number
	const carId =
		await http
			.put('/events/history/global',
				 "(events[eventName='NEW' and event.Dvla.VRM='LF55HDC'].sourceId)[0]",
				 'text/plain')
			.then(result => {
			        console.log("testAddKeeper-testPut:OK ---- ");
			        jsonResult = JSON.parse(result);
			        console.log(jsonResult)
			        return jsonResult.data ;
			    })
		    .catch(err => {
		    	console.log("testPost:FAIL ---- ");
		    });

	console.log(`carId --- ${carId} ---- `);
	await http
		.post(`/events/add/keeper/${carId}`,
			  { Keeper: { name:"Fred Bloggs",address:"1 the Street, Townville, UK1 3TO"}})
		.then(result => {
		        userDetails = result;
		        console.log("testPost:OK ---- ");
		        // Use user details from here
		        console.log(userDetails)
		        return userDetails ;
		    })
	    .catch(err => {
	    	console.log("testPost:FAIL ---- ");
	        console.log(err);
	    });
	
	// Colate both records
	// 
	const combined =
		await http
			.put(`/events/history/${carId}`,
				 "events[*]{ 'dvla' : event.Dvla , 'keeper' : event.Keeper }",
				 'text/plain')
			.then(result => {
			        console.log("testAddKeeper-testPut:OK ---- ");
			        jsonResult = JSON.parse(result);
			        console.log(jsonResult)
			        return jsonResult.data ;
			    })
		    .catch(err => {
		    	console.log("testPost:FAIL ---- ");
		    });
	
	console.log(combined);
}
async function testAll()
{
	console.log("------------------------------------------------");
	await testAddCar();
	console.log("------------------------------------------------");
	await testFindCar();
	console.log("------------------------------------------------");
	await testAddKeeper();
	console.log("------------------------------------------------");
}

testAll();