const fs = require('fs');
const myhttp = require('./http.js');
const http = new myhttp()
					.baseUrl('http://localhost:3000')

async function testAddSnapshot()
{
	console.log("testAddSnapshot");
	
	// First piece of test data
	// Matching the NEW record & with the correct Licence Number
	const carId =
		await http
			.put('/events/history/global',
				 "(events[eventName='NEW' and event.Dvla.VRM='LF55HDC'].sourceId)[0]",
				 'text/plain')
			.then(result => {
			        console.log("testAddSnapshot-testPut:OK ---- ");
			        jsonResult = JSON.parse(result);
			        console.log(jsonResult)
			        return jsonResult.data ;
			    })
		    .catch(err => {
		    	console.log("testAddSnapshot:FAIL ---- ");
		    });

	console.log(`testAddSnapshot -- carId --- ${carId} ---- `);
	// Colate both records
	// 
	const combined =
		await http
			.patch(`/events/history/${carId}`,
				 "events[inSnapshot=false]{ 'dvla' : event.Dvla , 'keeper' : event.Keeper }",
				 'text/plain')
			.then(result => {
			        console.log("testAddSnapshot-testPut:OK ---- ");
			        jsonResult = JSON.parse(result);
			        console.log(jsonResult)
			        return jsonResult.data ;
			    })
		    .catch(err => {
		    	console.log("testAddSnapshot:FAIL ---- ");
		    });
	
	console.log(combined);
}
async function testAll()
{
	console.log("------------------------------------------------");
	await testAddSnapshot();
	console.log("------------------------------------------------");
}

testAll();