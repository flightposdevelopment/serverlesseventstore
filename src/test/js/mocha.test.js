// TEST EVENT STORE
var assert = require('assert');
const myhttp = require('./http.js');

// Test against ?
const storeUrl = process.env.STORE_URL ? process.env.STORE_URL : 'http://localhost:3000/local';
const http = new myhttp()
					.baseUrl(storeUrl);


async function checkForServer() {

	var contacted = false ;
	var retries = 10 ;
	while (!contacted && retries>0) {

		console.log(`Checking ${storeUrl}.`)
		await http
				.get()
				.then(result => {
						contacted = true ;
						console.log("testGet:OK ---- ");
					})
				.catch(err => {
					console.log(`testGet:FAIL ---- ${err.message}`);
				});

		retries--;		

		if (!contacted && retries>0)
			await new Promise(resolve => setTimeout(resolve, 1000));
	}
	if ( !contacted ) {
		console.log(`testGet:FAIL ---- Server not UP`);
		process.exit(1); // exit with error
	}
}	


console.log(`Running tests against ${storeUrl}.`);
let run = false
before(async () => {  
    if ( run === true ) return
    console.log('GLOBAL ############################')
	await checkForServer();  
    run = true
  })

  

describe('Perform a GET', function () {
  it('should call the service', async function () {
    
    // WITH

	// WHEN
		await http
			.get('/health')
			.then(result => {
			        userDetails = result;
			        console.log("testGet:OK ---- ");
			        // Use user details from here
			        console.log(userDetails)
			    })
		    .catch(err => {
		    	console.log(`testGet:FAIL ---- ${err.message}`);
				throw err ;
		    });

	// THEN
		
  });
});

//-------------------------------------------------------------------
// Perform a POST Method
//
describe('Perform a POST', function () {
	  it('should call the service', async function () {
	    
	    // WITH

		// WHEN
		const rqData = { "foo" : "bar" };
		
		await http
			.post('/events/add/new',rqData)
			.then(response => {
					console.log("testPost:OK ---- ");
			
					// THEN
					if ( response.error ) throw new Error (`Request failed ${response.error}`);
					if ( !response.item ) throw new Error (`Request failed no item data`);
					if ( !response.item.sourceId ) throw new Error (`Request failed no item.sourceId data`);
				})
			.catch(err => {
				console.log(`testPost:FAIL ---- ${err.message}`);
				throw err ;
			});


			
	  });
	});

//-------------------------------------------------------------------
// Perform a POST Method get the ID and update
//
describe('Perform a POST & UPDATE', function () {
	it('should do a POST & UPDATE', async function () {
	  
	  // WITH

	  // WHEN
	const rqData1 = { "thing" : 1 };
	
	const rsData1 =
		await http
			.post('/events/add/new',rqData1)
			.then(response => {
					console.log("testPost:OK ---- ");
					console.log(JSON.stringify(response, null, 4))
					
					// THEN
					if ( response.error ) throw new Error (`Request failed ${response.error}`);
					if ( !response.item ) throw new Error (`Request failed no item data`);
					if ( !response.item.sourceId ) throw new Error (`Request failed no item.sourceId data`);

					return response ;
				})
			.catch(err => {
				console.log(`testPost:FAIL ---- ${err.message}`);
			    throw err ;
			});

	  // AND
	  const sourceId = rsData1.item.sourceId;
	  console.log(`testPost:RS id=${rsData1.item.sourceId} ---- `);
	  const rqData2 = { "thing" : 2 };
		  
	  const rsData2 =
		  await http
			  .post(`/events/add/update/${sourceId}`,rqData1)
			  .then(response => {
					
					// THEN
					if ( response.error ) throw new Error (`Request failed ${response.error}`);
					if ( !response.item ) throw new Error (`Request failed no item data`);
					if ( !response.item.sourceId ) throw new Error (`Request failed no item.sourceId data`);
					  
					// Use user details from here
					return response ;
				  })
			  .catch(err => {
				console.log(`testPost:FAIL ---- ${err.message}`);
			    throw err ;
			  });

	    
	});
  });
