const request = require('request');
var colors = require('colors'); // https://ourcodeworld.com/articles/read/298/how-to-show-colorful-messages-in-the-console-in-node-js

function myHttp()
{
	
	this.baseUrlValue = null ;

	this.baseUrl 
		= function (url)
		{
			this.baseUrlValue = url ;
			
			return this ;
		}
	

	this.get
		= function(path)
		{
		const fullUrl = [this.baseUrlValue, path].filter(Boolean).join("");
		console.log(`GET ${fullUrl}`.red)
		
		// Setting URL and headers for request
		var options = {
			url: fullUrl,
			headers: {
				'User-Agent': 'request'
			}
		};
		// Return new promise
		return new Promise(function(resolve, reject) {
			// Do async job
			request.get(options, function(err, resp, body) {
				if (err) {
					reject(err);
				} else {
					resolve(JSON.parse(body));
				}
			});
		});
	}
	

	this.post
	= function(path,jsonformData)
	{
	const fullUrl = [this.baseUrlValue, path].filter(Boolean).join("");
	console.log(`POST ${fullUrl}`.red)

	// Validate
	if ( !fullUrl ) throw new Error("No Url defined");
	if ( !jsonformData ) throw new Error("No jsonformData defined");
	
	// Setting URL and headers for request
	var options = {
		url: fullUrl,
		headers: {
			'User-Agent': 'request'
		},
		json: jsonformData  // JSON.stringify(jsonformData)
	};		
	// Return new promise
	return new Promise(function(resolve, reject) {
		// Do async job
		request.post(options, function(err, resp, body) {
			if (err) {
				reject(err);
			} else {
				console.log("-----------------------------------------------------");
				console.log(typeof body);
				console.log(body);
				console.log("-----------------------------------------------------");
				resolve(body);
			}
		});
	});		
	}	

	this.put
	= function(path,bodyData,contentType)
	{
	const fullUrl = [this.baseUrlValue, path].filter(Boolean).join("");
	console.log(`PUT ${fullUrl}`.red)

	// Validate
	if ( !fullUrl ) throw new Error("No Url defined");
	if ( !bodyData ) throw new Error("No bodyData defined");
	
	// Setting URL and headers for request
	var options = {
		url: fullUrl,
		headers: {
			'User-Agent': 'request'
		}
	};		
	
	// Alter the default from Json.
	if (contentType) 
		{
		options.headers['Content-Type'] = contentType ;
		options.body  = bodyData ;
		}
	else
		{
		options.json = bodyData ;
		}
	
	console.log("HTTP (Options) -----------------------------------------------------".cyan);
	console.log(options);
	
	// Return new promise
	return new Promise(function(resolve, reject) {
		// Do async job
		request.put(options, function(err, resp, body) {
			if (err) {
				reject(err);
			} 
			else if ( resp.statusCode != 200 )
				{
				reject(new Error("Failed"));
				}
			else {
				console.log("HTTP (Rs) -----------------------------------------------------".cyan);
				console.log(typeof body);
				console.log(body);
				console.log("HTTP (Rs) -----------------------------------------------------".cyan);
				resolve(body);
			}
		});
	});		
	}
	
	this.patch
		= function(path,bodyData,contentType)
		{
		const fullUrl = [this.baseUrlValue, path].filter(Boolean).join("");
		console.log(`PATCH ${fullUrl}`.red)

		// Validate
		if ( !fullUrl ) throw new Error("No Url defined");
		if ( !bodyData ) throw new Error("No bodyData defined");
		
		// Setting URL and headers for request
		var options = {
			url: fullUrl,
			headers: {
				'User-Agent': 'request'
			}
		};		
		
		// Alter the default from Json.
		if (contentType) 
			{
			options.headers['Content-Type'] = contentType ;
			options.body  = bodyData ;
			}
		else
			{
			options.json = bodyData ;
			}
		
		console.log("HTTP (Options) -----------------------------------------------------".cyan);
		console.log(options);
		
		// Return new promise
		return new Promise(function(resolve, reject) {
			// Do async job
			request.patch(options, function(err, resp, body) {
				if (err) {
					reject(err);
				} 
				else if ( resp.statusCode != 200 )
					{
					reject(new Error("Failed"));
					}
				else {
					console.log("HTTP (Rs) -----------------------------------------------------".cyan);
					console.log(typeof body);
					console.log(body);
					console.log("HTTP (Rs) -----------------------------------------------------".cyan);
					resolve(body);
				}
			});
		});		
		}
	
}; // end myHttp 

module.exports = myHttp;
