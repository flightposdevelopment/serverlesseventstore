'use strict';
const eventHandler = require('../../main/js/process/streamHandler.js');
const testEvent = require('./testEvent');  // including test event
const sinon = require('sinon'); // this guy is needed to use spied and mocks. More about spy is here https://sinonjs.org/releases/latest/spies/
var assert = require('chai').assert; //this guys is needed cause by default there is no normal asserts (like assertEquals, assertTrue) refer https://www.chaijs.com/api/assert/
var AWS = require('aws-sdk-mock'); //Even having sinon there is no way to mock AWS services without this special library. refer https://www.npmjs.com/package/aws-sdk-mock


describe('When dynamodb event is coming', function () {
  it('should send correct message to SNS with subject, message and topicArn', async function () {

    // WITH
    let event = testEvent;  // using test event which sits next to the test file
    let snsSpy = sinon.spy(); // creating a spy
    AWS.mock('SNS', 'publish', snsSpy);  //replacing real AWS SNS with a spy

    // WHEN
    eventHandler.main(event, {}, function(){});


    // THEN
    assert.ok(snsSpy.calledOnce, 'should publish a message once');  // first assure there was only one invocation
    let actualMessage = snsSpy.args[0][0]; // Spy can capture arguments it was called with but we have to unwrap them that's why [0][0]
    //checking that general structure is correct and not null
    assert.exists(actualMessage.Subject);
    assert.exists(actualMessage.Message);
    assert.exists(actualMessage.TopicArn);

    let actualParams = JSON.parse(actualMessage.Message); // when we are sure there is a message we can parse it
    //checking that internal message is correctly formed
    assert.exists(actualParams.sourceId);
    assert.exists(actualParams.createdAt);
    assert.exists(actualParams._ref);
    assert.exists(actualParams.eventName);

    assert.equal(`eventstore::${actualParams.eventName}`, actualMessage.Subject);  //checking that subject has correct structure
    assert.equal(`/events/history/${actualParams.sourceId}/${actualParams.createdAt}`, actualParams._ref); //checking that url was formed correctly

  });

  afterEach(() => {
    // Restore the default sandbox here. otherwise it could lead to a memory leak as said in documentation.
    AWS.restore('SNS', 'publish');
  });

});