
var AWS = require('aws-sdk'); 
var util = require('util');
const fs = require('fs');
var text = fs.readFileSync('config.json','utf8');
var config = JSON.parse(text);

console.log(`--- Config `, config );

// Are we working on an AWS profile
if ( config.profile ) {
  console.log(`--- Profile ${config.profile}`);
  var credentials = new AWS.SharedIniFileCredentials({"profile": config.profile });
  AWS.config.credentials = credentials;  
}


var sns = new AWS.SNS(config.snsParams);

//-------------------------------------------------------------------------------------------------------
function publishText(msg) {
  var publishParams = { 
    TopicArn : config.TopicArn,
    Message: msg,
    Subject: "TEST TXT MESSAGE",
  };

  sns.publish(publishParams, function(err, data) {
    //process.stdout.write(".");
    console.log("------------------------------------------------------------------------");
    if ( data ) {
      console.log(`--- Publish return data  `, JSON.stringify(data) );
      console.log(`Topic:${config.TopicArn} MessageId:${data.MessageId}`);  
    }
    if ( err ) {
      console.log(`--- Publish return data  `, JSON.stringify(err) );
      console.log(`Topic:${config.TopicArn} err: ${err.message}` );  
    }
  });
}
//-------------------------------------------------------------------------------------------------------
function publishJson(msg) {

  var item = { "foo" : "bar" , "msg": msg };
  //var defaultMessage = { "default": item };
  var defaultMessage = JSON.stringify(item);

  var publishParams = { 
    TopicArn : config.TopicArn,
    Message: defaultMessage ,
//    MessageStructure: "json",
    Subject: "TEST JSON MESSAGE",
  };

  sns.publish(publishParams, function(err, data) {

    if ( err ) {
      console.log("------------------------------------------------------------------------");
      console.log(err);

    }
    else {
    //process.stdout.write(".");
    console.log("------------------------------------------------------------------------");
    console.log(`--- Publish return data  `, JSON.stringify(data) );
    console.log(`Topic:${config.TopicArn} MessageId:${data.MessageId}`);

    }
  });
}

//-------------------------------------------------------------------------------------------------------
for (var i=0; i < 3; i++) {
  publishText("Text message: " + i);
}

//-------------------------------------------------------------------------------------------------------
for (var i=3; i < 6; i++) {
  publishJson("Json message: " + i);
}