var AWS = require('aws-sdk'); 
var util = require('util');
var async = require('async');
var fs = require('fs');
var text = fs.readFileSync('config.json','utf8');
var config = JSON.parse(text);

console.log(`--- Config `, config );
console.log(`--- Region ${config.region}`);

// Are we working on an AWS profile
if ( config.profile ) {
  console.log(`--- Profile ${config.profile}`);
  var credentials = new AWS.SharedIniFileCredentials({"profile": config.profile });
  AWS.config.credentials = credentials;  
}

var sns = new AWS.SNS(config.snsParams);
var sqs = new AWS.SQS(config.sqsParams);

function snsDeleteQueue(cb) {

  console.log("--- snsDeleteQueue");

  if (!config.QueueUrl)
  {
    console.log("Already deleted");
     cb();
     return ;
  }

  console.log(`QueueUrl=${config.QueueUrl}`);

  var params = {
    QueueUrl: config.QueueUrl
  };
  sqs.deleteQueue(params, function(err, data) {

    if (err !== null) {
      console.log(util.inspect(err));
      return cb(err);
    }

    delete config.QueueArn ;
    delete config.QueueUrl ;
    cb();

  });

} // End snsDeleteQueue


function snsDeleteTopic(cb) {

  if (config.TopicKeep) {
    console.log("--- Keeping Topic");
    delete config.TopicArn ;
    delete config.TopicKeep;
    cb();
    return ;
  }

  console.log("--- DeleteTopic");

  if (!config.TopicArn)
  {
    console.log("Already deleted");
     cb();
     return ;
  }

  console.log(`TopicArn=${config.TopicArn}`);

  var params = {
    TopicArn: config.TopicArn
  };
  sns.deleteTopic(params, function(err, data) {

    if (err !== null) {
      console.log(util.inspect(err));
      return cb(err);
    }

    delete config.TopicArn ;
    delete config.TopicKeep;
    cb();

  });

} // End snsDeleteTopic


function snsUnsubscribe(cb) {

  console.log("--- Unsubscribe");

  if (!config.SubscriptionArn)
  {
    console.log("Already unsubscribed");
     cb();
     return ;
  }

  console.log(`SubscriptionArn=${config.SubscriptionArn}`);

  var params = {
    SubscriptionArn: config.SubscriptionArn
  };
  sns.unsubscribe(params, function(err, data) {

    if (err !== null) {
      console.log(util.inspect(err));
      return cb(err);
    }

    delete config.SubscriptionArn ;
    cb();

  });

} // End snsUnsubscribe

function writeConfigFile(cb) {

  console.log("--- Save Config");


  fs.writeFile('config.json', JSON.stringify(config, null, 4), function(err) {
    if(err) {
      return cb(err);
    }

    console.log("config saved to config.json");
    cb();
  }); 

} // end writeConfigFile

//async.series([createTopic, createQueue, getQueueAttr, snsSubscribe, setQueueAttr, writeConfigFile]);
async.series([snsDeleteQueue,snsUnsubscribe,snsDeleteTopic,writeConfigFile]);