const AWS = require('aws-sdk'); 
const util = require('util');
const async = require('async');
const fs = require('fs');

const REGION= 'us-east-1';
const REGION_PROFILE= 'eu-west-2';
const TOPIC= 'eventstore-testing';
const SLS_INSTANCE = '123456789012';

console.log(`--- CREATE ---`);
var profile ;
var existingTopicArn ;
var localstack = false ;
var slsOffline = false ;

for (let j = 0; j < process.argv.length; j++) {

  //console.log(j + ' -> ' + (process.argv[j])); 
  if ( "--localstack" == process.argv[j] )
    localstack = true ;

    if ( "--slsOffline" == process.argv[j] )
    slsOffline = true ;

  if ( "--profile" == process.argv[j] )
    profile=process.argv[j+1];

  if ( "--existingTopicArn" == process.argv[j] )
    existingTopicArn=process.argv[j+1];
}

// Set up a config object to be saved.
var config = null;
try {
  config = require('./config.json');
} catch (e) {
  config = {};
}



// Are we working on an AWS profile
config.region = REGION ;
if ( profile ) {
  config.profile = profile ;
}

console.log(`\t`,`Config `, config );


if ( config.profile ) {
  console.log(`\t`,`Getting credentials for profile ${config.profile}`);
  var credentials = new AWS.SharedIniFileCredentials({"profile": config.profile });
  AWS.config.credentials = credentials;  
  config.region = REGION_PROFILE ;
}





// Set Up connection to AWS/Localstack
const snsParams = {
  region: config.region
};
const sqsParams = {
  region: config.region
};

if ( localstack || slsOffline ) {
  // Add in local stack
  snsParams.endpoint = 'http://127.0.0.1:4575'; // Local stack URL
  sqsParams.endpoint = 'http://127.0.0.1:4576'; // Local stack URL
}

// Set up for save
config.snsParams = snsParams ;
config.sqsParams = sqsParams ;

if ( slsOffline ) {
  existingTopicArn = `arn:aws:sns:${config.region}:${SLS_INSTANCE}:${TOPIC}`
}


if ( existingTopicArn ) 
  console.log(`--- Using Topic ${existingTopicArn}`);


var sns = new AWS.SNS(snsParams);
var sqs = new AWS.SQS(sqsParams);



function listTopics(cb){

  if ( !existingTopicArn ) {
    console.log(`\t`,`listTopics - no existing topic specified`);
    cb();
    return;
    }

  if ( slsOffline ) {
    console.log(`\t`,`List topics sls Offline is on ${existingTopicArn}`);
    cb();
    return;
  }


  if ( existingTopicArn ) {

    console.log(`\t`,`List topics to find existing ${existingTopicArn}`);
    var params = {
    };

    sns.listTopics(params, function(err, data) {

      if (err !== null) {
        console.log(util.inspect(err));
        return cb(err);
      }

      console.log(`\t`,`List topics returned:`, JSON.stringify(data) );

      // Find the topic
      var found = false ;
      if ( data.Topics ) {
        data.Topics.forEach(element => {
      
          console.log(element.TopicArn)
          if ( element.TopicArn == existingTopicArn ) found = true ;
  
        });  
      }

      if (found) {
        cb();
      }
      else {
        console.log(`\t`,`List topics did not find existing: ${existingTopicArn}`);
        cb(new Error(`List topics did not find existing: ${existingTopicArn}`));
  
      }
    });
  
  }

}


function createTopic(cb) {

  if ( existingTopicArn )
    {
      console.log(`--- createTopic - Existing Topic ${existingTopicArn}`);
      config.TopicArn = existingTopicArn;
      config.TopicKeep = true;
  
      cb();
      return ;
      }

  console.log("--- Create Topic");
  if (profile) console.log(`profile=${profile}`);

  sns.createTopic({
    'Name': TOPIC
  }, function (err, result) {

    if (err !== null) {
      console.log(util.inspect(err));
      return cb(err);
    }
    //console.log(util.inspect(result));

    config.TopicArn = result.TopicArn;
    delete config.TopicKeep;
    console.log(`TopicArn=${result.TopicArn}`);

    cb();
  });
}

function createQueue(cb) {

  if ( slsOffline ) {
    console.log(`--- NO Queue sls Offline is on.`);
    cb();
    return;
  }
  console.log("--- Create Queue");

  sqs.createQueue({
    'QueueName': TOPIC
  }, function (err, result) {

    if (err !== null) {
      console.log(util.inspect(err));
      return cb(err);
    }

    //console.log(util.inspect(result));

    config.QueueUrl = result.QueueUrl;
    console.log(`QueueUrl=${result.QueueUrl}`);

    cb();

  });

}


function getQueueAttr(cb) {

  if ( slsOffline ) {
    console.log(`--- NO Queue sls Offline is on.`);
    cb();
    return;
  }

  console.log("--- Get Queue Attr");

  sqs.getQueueAttributes({
    QueueUrl: config.QueueUrl,
  AttributeNames: ["QueueArn"]
  }, function (err, result) {

    if (err !== null) {
      console.log(util.inspect(err));
      return cb(err);
    }

    //console.log(util.inspect(result));

    config.QueueArn = result.Attributes.QueueArn;
    console.log(`QueueArn=${config.QueueArn}`);

    cb();

  });

}


function snsSubscribe(cb) {

  console.log("--- Subscribe");

  sns.subscribe({
    'TopicArn': config.TopicArn,
  'Protocol': 'sqs',
  'Endpoint': config.QueueArn
  }, function (err, result) {

    if (err !== null) {
      console.log(util.inspect(err));
      return cb(err);
    }

    //console.log(util.inspect(result));
    console.log(`SubscriptionArn=${result.SubscriptionArn}`);
    config.SubscriptionArn = result.SubscriptionArn;

    cb();
  });

}

function setQueueAttr(cb) {

  if ( slsOffline ) {
    console.log(`--- NO Queue sls Offline is on.`);
    cb();
    return;
  }

  console.log("--- Set Queue Attr");

  var queueUrl = config.QueueUrl;
  var topicArn = config.TopicArn;
  var sqsArn = config.QueueArn;
  var id = sqsArn + "/SQSDefaultPolicy";

  console.log(id);

  var attributes = {
    "Version": "2008-10-17",
    "Id": sqsArn + "/SQSDefaultPolicy",
    "Statement": [{
      "Sid": "Sid" + new Date().getTime(),
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "SQS:SendMessage",
      "Resource": sqsArn,
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": topicArn
        }
      }
    }
    ]
  };

  sqs.setQueueAttributes({
    QueueUrl: queueUrl,
    Attributes: {
      'Policy': JSON.stringify(attributes)
    }
  }, function (err, result) {

    if (err !== null) {
      console.log(util.inspect(err));
      return cb(err);
    }

//    console.log(util.inspect(result));
    console.log("Done");

    cb();
  });

}

function writeConfigFile(cb) {

  console.log("--- Save Config");


  fs.writeFile('config.json', JSON.stringify(config, null, 4), function(err) {
    if(err) {
      return cb(err);
    }

    console.log("config saved to config.json");
    cb();
  }); 

}


async.series([listTopics, createTopic, createQueue, getQueueAttr, snsSubscribe, setQueueAttr, writeConfigFile]);