var AWS = require('aws-sdk'); 
var util = require('util');
const fs = require('fs');
var text = fs.readFileSync('config.json','utf8');
var config = JSON.parse(text);

console.log(`--- Config `, config );

// Are we working on an AWS profile
if ( config.profile ) {
  console.log(`--- Profile ${config.profile}`);
  var credentials = new AWS.SharedIniFileCredentials({"profile": config.profile });
  AWS.config.credentials = credentials;  
}

var sqs = new AWS.SQS(config.sqsParams);

console.log(`--- Queue ${config.QueueUrl}`);

function receiveMessage() {

  if (!config.QueueUrl)
  {
    console.log("No Queue");
     return ;
  }

  

  var receiveMessageParams = {
    QueueUrl: config.QueueUrl,
    MaxNumberOfMessages: 10
  };

  sqs.receiveMessage(receiveMessageParams, receiveMessageCallback);
}

function receiveMessageCallback(err, data) {
  // console.log(data);

  var waiting = false ;

  if (data && data.Messages && data.Messages.length > 0) {

    for (var i=0; i < data.Messages.length; i++) {

      // do something with the message here..."
      
      var message = data.Messages[i];
      //console.log("----------------------\n",message)

      var body = JSON.parse(message.Body);
      console.log("----------------------\n",body);
//      body.Subject = JSON.parse(body.Subject);
      console.log("----------------------------\n",`SUBJECT: ${body.Subject}`);
      console.log("----------------------------\n",`MESSAGE: ${body.Message}`);

      try {
        body.Message = JSON.parse(body.Message);
        console.log("----------------------\n","Message\n",JSON.stringify(body.Message, null, 4));
      } catch  {
        console.log("BODY NOT JSON");
      }
      

    if ( body.Message.eventName ) console.log("----------------------\n","Message:eventName",body.Message.eventName);
    if ( body.Message.sourceId )  console.log("Message:sourceId",body.Message.sourceId);
    if ( body.Message.createdAt ) console.log("Message:createdAt",body.Message.createdAt);
  

      //
      // Delete the message when we've successfully processed it
      var deleteMessageParams = {
        QueueUrl: config.QueueUrl,
        ReceiptHandle: data.Messages[i].ReceiptHandle
      };

      sqs.deleteMessage(deleteMessageParams, deleteMessageCallback);
    }

    receiveMessage();

  } else {

    if ( waiting ) {
      process.stdout.write("?");
    } else {
      process.stdout.write(".");
      waiting = true ;
    }
    setTimeout(receiveMessage, 1000);
  }
}

function deleteMessageCallback(err, data) {
//  console.log("deleted message");
  process.stdout.write("X");
  //console.log(data);
}

console.log("Listening:");
receiveMessage();
//setTimeout(receiveMessage, 5000);

