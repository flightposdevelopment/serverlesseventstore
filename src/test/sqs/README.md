# Eventstore - test Stack

## Purpose

To create a Localstack with SNS and SQS to facliitate testing of the event store.

## Setup

Run this script to

1. Start the docker container detached.
2. Create the SQS and its SNS topic


```
docker-compose up -d
node create.js
```

## Send some Events - PUBLISH

This script populates the queue with events that are then passed on to the SQS for reading.

```
node publish.js
```

## Read the messages - SUBSCRIBE/CONSUME

To read the messages run this program:

```
node consume.js
```

This will then sit in place listening until stopped.
More messages can be sent easily through the `node publish.js`.

## Localstack tricks

### Create topic

```
aws --endpoint-url=http://localhost:4575 sns create-topic --name my_topic
```



### Listing the Topics on your server.

```
aws --endpoint-url=http://localhost:4575 sns list-topics
```

### Create Queue:

```
aws --endpoint-url=http://localhost:4576 sqs create-queue --queue-name my_queue
```

### List Queue:

```
aws --endpoint-url=http://localhost:4576 sqs list-queues
```

### Subscribe Queue to Topic:

```
aws --endpoint-url=http://localhost:4575 sns subscribe --topic-arn arn:aws:sns:us-east-1:123456789012:my_topic --protocol sqs --notification-endpoint arn:aws:sns:us-east-1:123456789012:my_queue
```

### List subscriptions:

```
aws --endpoint-url=http://localhost:4575 sns list-subscriptions
```

After creating our infra we need to start sending messages so that our service can receive and use them. For that we are going to publish the message to the topic as followed:

```
$ aws --endpoint-url=http://localhost:4575 sns publish --topic-arn arn:aws:sns:us-east-1:123456789012:my_topic --message "Hi"
```

If you want it’s possible to send the content of a file too:

```
$ aws --endpoint-url=http://localhost:4575 sns publish --topic-arn arn:aws:sns:us-east-1:123456789012:my_topic --message file://file.json
```

For other usefull stuff on Localstack see: https://gugsrs.com/localstack-sqs-sns/