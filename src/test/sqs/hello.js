var AWS = require("aws-sdk"); // must be npm installed to use

const PORT=4575;
const TOPIC= 'demo';
// The ARN and instance id seem to be fixed to 123456789012 & us-east-1
const REGION= 'us-east-1';
const INSTANCE = '123456789012';

var sns = new AWS.SNS({
  endpoint: `http://127.0.0.1:${PORT}`,
  region: REGION,
});
sns.publish({
  Message: "++++++++++hello!+++++++++++",
  MessageStructure: "json",
  TopicArn: `arn:aws:sns:${REGION}:${INSTANCE}:${TOPIC}`,
}, () => {
  console.log("ping");
});