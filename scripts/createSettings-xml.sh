#!/bin/bash

if ! command -v envsubst &> /dev/null
then
    echo "envsubst could not be found"
    apt-get update
    apt-get -y install gettext-base
fi

envsubst < scripts/maven-settings-xml.template >> settings.xml