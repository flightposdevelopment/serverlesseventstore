#!/bin/bash

if [ -z "$AWS_ACCESS_KEY_ID" ]
then
      echo "\$AWS_ACCESS_KEY_ID is empty"
      exit 1
fi
if [ -z "$AWS_SECRET_ACCESS_KEY" ]
then
      echo "\$AWS_SECRET_ACCESS_KEY is empty"
      exit 1
fi


# JAVA 8.* & Maven 3.*
IMAGE=flightpos/serverless:node16-jdk11
echo Starting Docker to run ${IMAGE} -----------------------------------
echo running maven with params "$@"
echo USING AWS_ACCESS_KEY_ID = [$AWS_ACCESS_KEY_ID]
echo ----------------------------------------------------------------------------

echo HOST ---
# WSL1 
##export WSL_PWD=$(echo $PWD | sed 's/\/mnt//')
# WSL 2
export WSL_PWD=$PWD
export MVN_DIR=/c/Users/${USER}

echo Project folder: $WSL_PWD
echo Maven folder  : $MVN_DIR

echo IMAGE ---
export DOCK_WORK_DIR=/usr/src/project
export DOCK_MVN_DIR=/var/maven

echo Work folder: $DOCK_WORK_DIR
echo Maven folder  : $DOCK_MVN_DIR



bashCmd=""
if [ -z "$1" ]
then
	echo Starting Dockerized Shell ----------------
	bashCmd="bash"
else   
	echo Starting Dockerized $@ ----------------
fi

# Docker params
# -i		 Keep STDIN open even if not attached
# -t		 Allocate a pseudo-TTY
# --rm		 Automatically remove the container when it exits
# MAVEN_OPTS Connects to the parent Maven host
docker run -it --rm \
		-p 3000:3000 \
		-p 3002:3002 \
		-p 8080:8080 \
		-e DOCKER_HOST=tcp://host.docker.internal:2375 \
		-e MAVEN_OPTS="-Dmaven.repo.local=${DOCK_MVN_DIR}/.m2/repository" \
		-v ${WSL_PWD}:${DOCK_WORK_DIR} \
		-v ${MVN_DIR}/.m2:${DOCK_MVN_DIR}/.m2 \
		-w ${DOCK_WORK_DIR} \
		--env AWS_ACCESS_KEY_ID \
		--env AWS_SECRET_ACCESS_KEY \
		--env AWS_DEFAULT_REGION \
		${IMAGE} \
		$bashCmd $@


