#!/bin/bash -f

# Colours : https://misc.flogisoft.com/bash/tip_colors_and_formatting
NC="\e[39m"
BLUE="\e[34m"
LGRAY="\e[37m"
BOLD="\e[1m"
DIM=" \e[2m"


usage()
{
echo -e ${BOLD}${BLUE} +++ AWSCREDS +++${NC}
cat << EOF
usage: $0 [-p name] [-q]

Fetches the properties from the aws creds file.
OPTIONS:
   
   -p      User profile
   -q      Quiet
   -h      Usage
   
EOF
}

verbose=1
profile=""
while getopts "h?qp:" opt; do

    case "$opt" in
    h|\?)
        usage
        exit
        ;;
    q)  verbose=0
        ;;
    p)  
    	profile="--profile $OPTARG"
    	echo "Fetching for profile $OPTARG"
        ;;
    esac
done

if [ $verbose = 1 ];
then
	echo
	echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
	echo -e ${BOLD}${BLUE} +++ AWSCREDS +++${NC}
	echo -e ${LGRAY}---------------------------------------------------------------------------------${NC}
	echo
fi


export AWS_REGION=$(aws configure get region $profile) 
export AWS_ACCESS_KEY_ID=$(aws configure get aws_access_key_id $profile) 
export AWS_SECRET_ACCESS_KEY=$(aws configure get aws_secret_access_key $profile) 

# Now fetch the account ID
export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --output text --query 'Account')

echo Writing to '.env'
echo "AWS_ACCOUNT_ID=${AWS_ACCOUNT_ID}" > .env
echo "AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}" >> .env
echo "AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}" >> .env
echo "AWS_REGION=${AWS_REGION}" >> .env
cat .env