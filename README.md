# Serverless Event Store

## What is it?

An event store is a type of database optimized for storage of events.

Conceptually, in an event store, only the events of a ledger.
The idea behind it is that the state or the ledger can be derived from these events. 
The events (and their corresponding data) are the only "real" facts that should be stored in the database.
The instantiation of all other objects can be derived from these events. The code instantiates these objects in memory. In an event store database, this means that all objects that should be instantiated, are not stored in the database. Instead these objects are instantiated 'on the fly' in memory by the code based on the events. After usage of these objects (e.g. shown in a user interface), the instantiated objects are removed from memory.

A crucial part of an event store database is that events that are stored are not allowed to be changed. Once stored, also erroneous events are not changed anymore. The only way to change (or better: correct) these events is to instantiate a new event with the new values. A correcting event would have the new values of the original event, with an event data of that corrected event, but a different transaction date. 
This mechanism ensures reproducibility at each moment in the time, even in the time period before the correction has taken place. It also allows to reproduce situations based on erroneous events (if required).

One advantage of the event store concept is that handling the effects of back dated events (events that take effect before previous events and that may even invalidate them) is much easier. In regular databases, handling backdated events to correct previous, erroneous events can be painful as it often results in rolling back all previous, erroneous transactions and objects and rolling up the new, correct transactions and objects. In an event store, only the new event (and it's corresponding facts) are stored. The code will then redetermine the transactions and objects based on the new facts in memory.

An event store will simplify the code in that rolling back erroneous situations and rolling up the new, correct situations is not needed anymore.

Disadvantage may be that the code needs to re-instantiate all objects in memory based on the events each time a service call is received for a specific 'record' of the ledger.

## The moving parts

![Diagram](/docs/system.png "The system layout")

The system is built & deployed using [Serverless.com](https://serverless.com/) and deployed to [AWS](https://aws.amazon.com/).

It comprises four parts (not counting permissions).

- *The API gateway* - Used to send events to the store.
- *The Lambdas* - Which process the requests.
- *Dynamodb* - Which stores the History or ledger.
- *SNS* - Which emits the notifications of the events.

### Component Parts

![Diagram](/docs/aws.png "The components")

| Component | | Use |
|---|---|---|
| APi Gateway | | Controls http access to the event service. |
| Lambda | Crudl | Performs Microservice Create, Read, Update, Delete and List functions. On database operations an event is sent to the dynamodb and the EventNotifier |
| Lambda | Query | Using [Jsonnata](https://jsonata.org/) a query is sent to the database and the results returned.  
| Lambda | Snapshot | Using [Jsonnata](https://jsonata.org/) a query is created anda new event stored. All the used fields are marked as part of the snapshot. |
| Lambda | Actuator | A health and info actuator like the [SpringBoot actuators](https://spring.io/guides/gs/actuator-service/) | 
| DynamoDb | | Persistance for the event store. | 
| SNS | | Notification for event s within the service. | 

## Simple usage

After deploying to your account the service will have these endpoints:

| Method | PATH | Use |
|---|---|---
| GET | /health | This is a health check endpoint which has the same format as a [SpringBoot actuator](https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html).<br/> It will return a **500** if there is an issue.  
| GET | /info | This is also similar to a SpringBoot actuator.<br/>It returns information on the service.
| POST | /events/add/new | Sending Json to this creates an event of type **NEW** and returns an event ID (`sourceId`) which must be used on all subsequent requests for this stream.
| POST | /events/add/{eventName} | Like the one above this allows you to create an event of type `eventName` with a new `sourceId`.
| POST | /events/add/{eventName}/{sourceId} | If you wish to add an event to an existing stream then post to this endpoint.
| GET | /events/history/{sourceId}[?before=epocMilli[&after=epocMilli]] | Once a stream has been created this request will allow you to see the sequence of events for the `sourceId`.<br/> The **before** and **after** epoc flags allow us to search the stream.
| GET | /events/history/global[?before=epocMilli[&after=epocMilli]] | This is the same as the above but it returns **ALL** events for all time.
| GET | /events/history/{id}/{at} | This will return a single event at an epoc time.
| PUT | /events/history/{id}/{at} | This will return a single event at an epoc time.<br/> However the posted body text can be a JSONata query which is run on the event before it is returned.
| PUT | /events/history/{id} | This will return an entire event stream since its beginning.<br/> However the posted body text can be a JSONata query which is run on the event before it is returned.
| PUT | /events/history/global | This will return the entire world history stream.<br/> However the posted body text can be a JSONata query which is run on the event before it is returned.
| PATCH | /events/history/{id} | This is very simular to the **PUT** however it inserts a new event of type **SNAPSHOT** into the stream.
| PATCH | /events/history/global | As above but for the world.
| GET | /swagger | OpenId definition.

## Project Structure

There are two parts to this project.
This root project contains the tests and the `/serverless` folder contains the code.
To access the code please click here ... [CLICK ME](serverless/README.md).

## Running the tests

To get started with the tests just run:

```bash
npm i
```

Followed by

```bash
npm test
```

There is a block of 'test' code which will stuff the db with some data to query.
To run this call...

```bash
node src/test/js/http-stuff-db.js
```

## Populating the database

The database can be populated ("stuffed") with data that represents Card registration.
Do this start the [service](serverless/README.md) in a seperate window and then come back here to run this:

```bash
node src/test/js/http-snapshot-db.js
```

This adds a `NEW` and a `KEEPER` event node in the database.

### Adding a SNAPSHOT

A snapshot can be created by calling ...

```bash
node src/test/js/http-snapshot-db.js
```

## Running the service

The service can be started in a number of ways:

1. With an embedded Dynamodb
2. With an external Dynamodb
3. Within a docker compose

### Running embedded Dynamodb

Starts using dynamodb local. Please note that you still need to install DynamoDB Local first.
So check https://www.npmjs.com/package/serverless-dynamodb-local

```bash
npm run sls
```

### Running with an external Dynamodb

This is mostly for use with LocalStack or the docker-compose.

```bash
npm run sls:extdynamo
```

This will depend on two environment vars `LOCAL_DYNAMO_SERVER` and `LOCAL_DYNAMO_PORT` which default to `localhost` and `8080` respectively.

### Within docker-compose

This will start a copy of DynamoDB within the container and then attach the event store.
The purpose of this is to provide a template for adding eventstore to other docker environments.





## Basic Usage

The Event Store works by receiving requests requests to record information though its API. The CRUDL functions (CRUD + LIST) allow the calling process to have events recorded (blocks of JSON, unstructured) against an named event which the Eventstore simply takes from the name passed on the URL (`eventName`) and associates it with an event id (`sourceId`). This event is combined with a time in milliseconds which forms the full record.

The API can then at a later time be queried through the API to retrieve events for:

- A single  event id (`sourceId`)
- All events for a period of time
- A  event id (`sourceId`) over a period of time.

Alternatively, an eventstore client can listen for an SNS event and filter on that.

## Event notification

The dynamodb has an event stream that processes can listen to directly. This data is largly unintelligible ,without detailed knowledge of the fields dynamo is storing. It is simply a list of fields that have been added or changed in the dynamodb table.

The eventstore makes that stream useful on an SNS event. 

This is done by the eventstore `streamFunctions` which do the listening and the translation into useful events.

The SNS event generated by the eventstore has a `Subject` and a `Message`.

The message has this format:

`eventStore::[eventName]`

Where `eventName` comes from the API call (called eventType above).
The `subject` contains the event type so that listeners can filter on the events they are interested in.
See: https://aws.amazon.com/getting-started/tutorials/filter-messages-published-to-topics/

The message however is more usefull and is a JSON object and 
is as follows:

```json
{
    "sourceId": <The source systems Id>,
    "eventName": <String holding the `eventType`>,
    "createdAt": <UTC time of the event>,
    "_ref": <The url to retrieve the event ie. /events/history/{sourceId}/{createdAt}>
}

```

From this data the receiver can determine what they need to do.

## Serverless Event Store - Implementation

### Application

This application is a Serverless.com event store implemented on AWS using lambdas & dynmodb.
This project uses [serverless-dynamodb-local](https://github.com/99xt/serverless-dynamodb-local), where the plugin Requires:

* serverless@^1
* Java Runtime Engine (JRE) version 6.x or newer


### Getting started

```bash
npm i
```

### Updating Serverless

To get the version of Serveless you currently have installed run:

```bash
sls -ver
```

If you are not running at least `1.60` run:

```bash
 npm update -g serverless
```

### Running the service locally

#### Natively

To get started with the service there needs to be a local data base and a dynamodb so just run:

```bash
npm i
sls dynamodb install
npm run sls
```

*Note: It will potentially type a lot of blank lines as it installs.*

*Note: See [serverless-dynamodb-local](https://github.com/99xt/serverless-dynamodb-local).*


```bash
./run.sh
```

This will start build all the openapi documentation and start  dynamodb locally on [http://localhost:8000/shell] and the service its self will be started on [http://localhost:3000]

#### Within Docker

```bash
docker image remove -f --no-prune eventstore \
&& docker build -t eventstore . \
&& docker run -it -p 3000:3000 -p 8000:8000 eventstore
```

If you want to avoid tagging, docker build `-q` outputs nothing but the final image hash, which you can use as the argument to docker run, and add `--rm` to docker run if you want the container removed automatically when it exits, since it doesn't clutter the local image repository.

```bash
docker run --rm -it $(docker build -q .)
```

It is possible to run the application within the root folder using docker-compose:

```bash
docker-compose up  -d
```

#### Swagger notation

Part of the `./run.sh` is the creation of the swagger notation which is run using:

```bash
./build.sh
```

The `build.sh` file will create the docker files in a `.openapi` folder.

*At this time the swagger is not complete*

#### Running up with dynamo

To start up the service on its own without the swagger building:

```bash
IS_OFFLINE=1 sls offline start --migrate true
```

On start there will be a dynamodb service on `http://localhost:8000/shell` and the service will be on `http://localhost:3000`.

### Sending a request to add a new event

This request will add a single new event to the service:

```bash
curl --location --request POST 'http://localhost:3000/events/add/new' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "foo": "bar"
}'
```

This will give you a response like this:

```json
{
    "item": {
        "sourceId": "5fa94b97-8c70-4b45-b23f-7bfa2159c44f",
        "eventName": "NEW",
        "event": {
            "foo": "bar"
        },
        "inSnapshot": false,
        "createdAt": 1579431719804,
        "shapshotAt": 1579431719804,
        "updatedAt": 1579431719804
    },
    "_dbg": {
        "TableName": "eventstore-events-dev"
    }
}
```

From which you can extract the id of the event `"sourceId": "5fa94b97-8c70-4b45-b23f-7bfa2159c44f"` and use it in a GET to retrieve the event:

```bash
curl --location --request GET 'http://localhost:3000/events/history/5fa94b97-8c70-4b45-b23f-7bfa2159c44f' 
```

More events can be added to this id using this call:

```bash
curl --location --request POST 'http://localhost:3000/events/add/event1/5fa94b97-8c70-4b45-b23f-7bfa2159c44f' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "thing": 1
}'
```

Now when you do the get you will have two events.

### Debugging the system

#### Locally

It is possible to run a local debugging session using VScode.
However, it is not possible to have dynamodb running under vscode as it is probably too much.
Therfore run the system as normal from the command line using `npm run sls` and then run the debbuger in vscode.
This session will be on port 3001 (and the command-line one on 3000).

#### On AWS

To debug the events being produced there is test code in `/src/test/remote`.

The steps are :

1. Deploy the stack: `sls deploy --stage xxx`
2. List all the topics in AWS: `aws sns list-topics`
3. Look for the `TopicArn` for the task you just deployed. The topic will be of this form `arn:aws:sns:eu-west-2:091105038475:eventstore-events-xxx` where `xxx` is the stage.
4. Move to the test folder: `/src/test/remote`
5. Create a queue to recieve the SNS events: `node ./create.js --TopicArn arn:aws:sns:eu-west-2:091105038475:eventstore-events-xxx`<br/> Where the parameter is the topic arn passed.
6. You now have a SQS you can listen to with so send an event to the store, using the `NEW` function: `curl --location --request POST 'https://f9szn1xz1c.execute-api.eu-west-2.amazonaws.com/bruce/events/add/new' --header 'Content-Type: text/plain' --data-raw '{
    "foo": "bar"
}'`
7. Now run the consumer: `node ./consume.js`

From this you should receive events called `eventstore::NEW` and with this content:

```json
 {
  sourceId: '11c5e399-87e4-4e67-8f6c-c4cd2b21b73c',
  eventName: 'NEW',
  createdAt: '1579451340596',
  _ref: '/events/history/11c5e399-87e4-4e67-8f6c-c4cd2b21b73c/1579451340596'
}
```

### Populating the database

The database can be populated ("stuffed") with data that represents Card registration.
Do this by performing the following:

```bash
node src/test/js/http-snapshot-db.js
```

This adds a `NEW` and a `KEEPER` event node in the database.

#### Adding a SNAPSHOT

A snapshot can be created by calling ...

```bash
node src/test/js/http-snapshot-db.js
```

